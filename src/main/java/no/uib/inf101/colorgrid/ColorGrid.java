package no.uib.inf101.colorgrid;

import no.uib.inf101.colorgrid.CellPosition;

import java.awt.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;



public class ColorGrid implements IColorGrid {

    private List<List<Color>> grid;
    private int rows;
    private int cols;
   public ColorGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            grid.add(new ArrayList<>());
            for (int j = 0; j < cols; j++) {
                grid.get(i).add(null);  
            }
         
        }

        
        
        

    }


    @Override
    public int rows() {
        
        return this.rows;
    }

    @Override
    public int cols() {
        
        return this.cols;
    }

    @Override
    public List<CellColor> getCells() {
    
    List<CellColor> list = new ArrayList<CellColor>();
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            CellPosition pos = new CellPosition(i, j);
            Color color = this.get(pos);
            CellColor cell = new CellColor(pos, color);
            list.add(cell);
        }
    }
    return list;
    }

    @Override
    public Color get(CellPosition pos) {
       return this.grid.get(pos.row()).get(pos.col());
        
    }

    @Override
    public void set(CellPosition pos, Color color) {
        this.grid.get(pos.row()).set(pos.col(), color);
        
        
    }
    
}
