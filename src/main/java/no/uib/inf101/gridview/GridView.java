package no.uib.inf101.gridview;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel {
  IColorGrid grid;

  private static final int MARGIN = 30;
  private static final Color BACKGROUND = Color.lightGray;

  public GridView(IColorGrid grid) {
  this.grid = grid;  
  this.setPreferredSize(new java.awt.Dimension(400, 300));
}
@Override
public void paintComponent(Graphics g){
  Graphics2D g2 = (Graphics2D) g;
  super.paintComponent(g2);
  drawGrid(g2);

  
 }

 void drawGrid(Graphics2D g2) {
  Rectangle2D box = new Rectangle2D.Double(MARGIN, MARGIN, getWidth()- MARGIN*2, getHeight()- MARGIN*2);
  g2.setColor(BACKGROUND);
  g2.fill(box);

  CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, grid, MARGIN);
  
  drawCells(g2, grid, converter);

    }
 private static void drawCells(Graphics2D g2, CellColorCollection cells, CellPositionToPixelConverter converter) {
    for (CellColor cp : cells.getCells()) {
      Rectangle2D bounds = converter.getBoundsForCell(cp.cellPosition());
      Color color = cp.color();
      g2.setColor(color);
      if (color == null) {
        g2.setColor(Color.DARK_GRAY);
      }
      g2.fill(bounds);
    }
      
  }

}



